class CmcService
  
  BASE_ENDPOINT = "https://api.coinmarketcap.com/v1/ticker/"
  
  def self.btc_usd
    request = Typhoeus.get("#{BASE_ENDPOINT}bitcoin/")
    
    response = JSON.parse(request.response_body)
    response[0]["price_usd"]
  end
  
end
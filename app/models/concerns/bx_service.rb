class BxService
  require 'openssl'
  require 'typhoeus'
  
  BX_KEY = ENV['BX_API_KEY']
  BX_SECRET = ENV['BX_API_SECRET']
  BX_BASE_ENDPOINT = "https://bx.in.th/api/"
  TRADEABLE = %w(ETH DAS REP BCH EVX GNO LTC OMG XRP XZC).freeze
  
  # DASH LISTED AS DAS ON BX, LISTED AS DASH ON BINANCE/BITTREX
  
  # PUBLIC API
  def self.get_market_data
    request = Typhoeus.get(BX_BASE_ENDPOINT)
    JSON.parse(request.response_body).with_indifferent_access
  end
  
  def self.get_pairings
    request = Typhoeus.get("#{BX_BASE_ENDPOINT}pairing/")
    JSON.parse request.response_body
  end
  
  def self.orderbook(pair_id)
    request = Typhoeus.get("#{BX_BASE_ENDPOINT}orderbook/?pairing=#{pair_id}")
    JSON.parse request.response_body
  end
  
  # PRIVATE API
  def self.generate_signature
    message = BX_KEY + get_nonce + BX_SECRET
    signature = OpenSSL::Digest::SHA256.new(message)
    
    return signature
  end
  
  def self.get_balances
    body = {
      key: BX_KEY,
      nonce: get_nonce,
      signature: generate_signature
    }.to_json
    
    request = Typhoeus::Request.new(
      "#{BX_BASE_ENDPOINT}balance/",
      method: :post,
      body: body,
      params: {},
      headers: { "Content-Type" => "application/json" }
    ).run
    
    JSON.parse request.response_body
  end
  
  def self.get_nonce
    DateTime.now.strftime('%Q')
  end
  
end
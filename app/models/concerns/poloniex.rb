class Poloniex
  require 'openssl'
  require 'Base64'
  
  POLONIEX_KEY = ENV['POLONIEX_KEY']
  POLONIEX_SECRET = ENV['POLONIEX_SECRET']
  TRADING_API = "https://poloniex.com/tradingApi"

  def self.ticker
    request = Typhoeus.get("https://poloniex.com/public?command=" + "returnTicker")
    JSON.parse request.response_body
  end
  
  def self.get_nonce
    DateTime.now.strftime('%Q')
  end
  
  def self.account_balances
    nonce = self.get_nonce
    request_url = "#{TRADING_API}?command=returnDepositAddress" +"&nonce=#{nonce}"
    headers = {
      'Sign' => self.signature,
      'Key' => POLONIEX_KEY
    }
    
    response = Typhoeus::Request.new(
      request_url,
      method: "post",
      body: '',
      headers: headers
    ).run
    response
  end  
  
  def self.signature(post_data='')
    OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), POLONIEX_SECRET, post_data)
  end
end

class BinanceService

  BINANCE_BASE_API = "https://api.binance.com/api/v1/"

  # NOT LISTED GNO

  def self.all_prices
    request = Typhoeus.get("#{BINANCE_BASE_API}ticker/allPrices")
    JSON.parse(request.response_body)
  end

  def self.price(currency)
    request = Typhoeus.get("#{BINANCE_BASE_API}ticker/24hr?symbol=#{currency.upcase}BTC")
    response = JSON.parse(request.response_body)
    response["askPrice"]
  end

  def self.price_usd(currency)
    satoshi = price(currency.upcase).to_f
    btc_price = CmcService.btc_usd.to_f

    satoshi * btc_price
  end

  def self.tradeable_markets
    prices = all_prices
    prices = prices.select{ |c| BxService::TRADEABLE.include? c["symbol"][0..2] }
                   .reject{|x| x["symbol"][3..-1] == "USDT" }
                   .reject{|x| x["symbol"][3..-1] == "BNB" }
  end

  def self.tradeable_btc_markets
    markets = tradeable_markets.reject{|x| x["symbol"][-3..-1] == "ETH" }
    btc_price = CmcService.btc_usd.to_f

    markets_with_usd = markets.each { |m|
      usd_price = m["price"].to_f * btc_price
      price_hash = { "usd_price" => usd_price }
      m.merge!(price_hash)
    }

    return markets_with_usd
  end

end

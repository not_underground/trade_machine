class CurrencyLayer
  require 'typhoeus'
  
  CL_KEY = ENV['CURRENCY_LAYER_KEY']
  CL_BASE_ENDPOINT = "http://apilayer.net/api/"
  
  def self.usd_thb
    request_url = "#{CL_BASE_ENDPOINT}live?access_key=#{CL_KEY}&currencies=THB"
    request = Typhoeus.get(request_url)
    
    response = JSON.parse(request.response_body).with_indifferent_access
    response[:quotes][:USDTHB]
  end  
  
  
end
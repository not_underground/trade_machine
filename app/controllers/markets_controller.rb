class MarketsController < ApplicationController

  def index
    @bx = @bx_pairings = BxService.get_market_data
    @bx_pairings = @bx_pairings.select{ |k,v| v[:primary_currency] == "THB" }
    @binance = BinanceService.tradeable_btc_markets
    @poloniex = Poloniex.ticker
    @btc = CmcService.btc_usd
    @thb = CurrencyLayer.usd_thb
  end

  def bx
    @bx_pairings = BxService.get_market_data
  end

end

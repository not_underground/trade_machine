Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # resource :bx, only: [:show, :index]
  #
  # root 'bx#index'
  resource :markets do
    get 'bx', to: 'markets#bx'
  end


  root 'markets#index'
end

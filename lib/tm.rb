class Tm
  
  def self.percent_increase(new, old)
    ((new - old) / old) * 100
  end
  
  def self.target(enter_price, percent)
    (enter_price + (enter_price * percent)).round(8)
  end
end
